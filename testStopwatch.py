#~/Programs/Python/pyTutorial/testStopwatch.py
#Matthew Ellison
# Created: 1-24-19
#Modified: 1-24-19
#This is a simple test script for the Stopwatch class

from Stopwatch import Stopwatch

#Start of the tests
timer = Stopwatch()

print("BEGIN TEST\n")
#Tests that should cause faults
print("Trying to print the time before it has been started:")
print(str(timer.getTime()))
timer.stop()
print("Trying to print the time after stop was called, but before start was:")
print(str(timer.getTime()))
timer.start()
print("Trying to print the time after it was started but before it was stopped:")
print(str(timer.getTime()))
cnt = 0
#A loop just to eat up time
print("\n\nEntering loop")
while(cnt < 10000):
	cnt = cnt + 1
print("Exiting loop\n")
timer.stop()
#Get the correct ending to the test
print("Trying to print time after it was finished:")
print(str(timer.getTime()))
print("\n")

#Test the different time resolutions
print("Print times in specific resolutions:")
print('{:08.6f}'.format(timer.getSeconds()) + " seconds")
print(str(timer.getMilliseconds()) + " milliseconds")
print(str(timer.getMicroseconds()) + " microseconds")
print(str(timer.getNanoseconds()) + " nanoseconds")


#Test end
print("\nEND OF TEST")

"""Results:
BEGIN TEST

Trying to print the time before it has been started:
-1
Trying to print the time after stop was called, but before start was:
-1
Trying to print the time after it was started but before it was stopped:
654000


Entering loop
Exiting loop

Trying to print time after it was finished:
5426600


Print times in specific resolutions:
0.005427 seconds
5.4266 milliseconds
5426.6 microseconds
5426600 nanoseconds

END OF TEST
"""
