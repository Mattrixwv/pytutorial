#This is a simple dice class. It can be initiated with any positive number of sides

import random

class Dice:
	#Setup all the variables needed and the random number generator
	def __init__(self, sides = 6):
		#Setup the highest number that can be rolled
		if(sides < 1):
			self.__sides = 6
		else:
			self.__sides = sides

		#Setup the other variables needed
		self.__face = 1	#The 'face' of the die that is used for scoring, etc

	#Get the number on the face of the die
	def getFace(self):
		return self.__face

	#Get the number of sides on the dice
	def getSides(self):
		return self.__sides

	#Get a new number in face
	def roll(self):
		self.__face = random.randint(1, self.__sides)
		return self.__face
