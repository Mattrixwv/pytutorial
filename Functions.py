#This is a simple function that uses functions to print text into the terminal


#Functions need to be defined before they are called

#This function prints a message
def printText1():
	print("This is printed in the first function")

#This function returns a string
def printText2():
	return "This is returned from the second function"

#This function sums two numbers and returns the value
def mySum(num1, num2):
	num = num1 + num2
	return num

#This is the main body of the program
#eval(intput()) returns an integer
num1 = eval(input("Enter the first number to be summed: "))
num2 = eval(input("Enter the second number to be summed: "))
printText1()
print(printText2())
#str(int) turns an integer to a string
print("This number is returned from the third function " + str(mySum(num1, num2)))
