import socket


print("Please enter an IP Address to scan.")
target = input("> ")

print("*" * 40)
print("* Scanning: " + target + " *")
print("*" * 40)

openPorts = []

for port in range(1, 1025):
	print("Port: " + str(port))
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	result = s.connect_ex((target, port))
	if result == 0:
		openPorts.append(result)
	s.close()

for port in openPorts:
	print("Port: " + str(port) + " Open")
