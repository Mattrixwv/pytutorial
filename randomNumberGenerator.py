#This is an example of a simpel random number generator

_BOTTOM_NUMBER = 1	#The lowest number to generate
_TOP_NUMBER = 101	#The number you want to generate below

import random

#Ask the user how many random numbers to generate
numbersToGenerate = int(input("How many random numbers do you want to generate? "))
print("I will generate " + str(numbersToGenerate) + " numbers between " + str(_BOTTOM_NUMBER) + " and " + str(_TOP_NUMBER))

#Use that variable to generate that many random numbers
for x in range(numbersToGenerate):
	print(str(random.randint(_BOTTOM_NUMBER, _TOP_NUMBER)))

#After testing it seems that the random number generator is seeded automatically
"""Results:
How many random numbers do you want to generate? 10
I will generate 10 numbers between 1 and 101
14
80
5
15
36
34
71
84
14
29
"""
