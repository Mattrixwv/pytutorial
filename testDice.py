#A simple script to test the Dice class

from Dice import Dice

#Try giving a negative number of sides
print("Testing for negative number of sides")
die = Dice(-3)
if(die.getSides() != 6):
	print("The die should have 6 sides, instead it has " + str(die.getSides()) + " sides.\nTest failed")
else:
	print("Test passed")

#Try the default constructor
die = Dice()
if(die.getSides() != 6):
	print("\nThe die should have 6 sides, instead it has " + str(die.getSides()) + " sides.\nTest failed")
else:
	print("Test passed")

#Try rolling it a few times and making sure the rolls are within expected boundaries
print("Testing the rolls of a default die")
die = Dice()
#Setup an array to keep track of how many times a number has been rolled
rolls = []
for x in range(die.getSides()):
	rolls.append(0)
#Roll 100 times the number of sides
for x in range(100 * die.getSides()):
	num = die.roll()
	rolls[num - 1] += 1		#You add one to the number directly behind the current number. This accounts for starting at subscript 0
#Print the results
cnt = 0
for num in rolls:
	print(str(cnt + 1) + ". " + str(num))
	cnt += 1


#Try using a many sided die and using the same tests as above
print("Testing the rolls of a die with a large number of sides")
die = Dice(50)
#Setup an array to keep track of how many times a number has been rolled
rolls = []
for x in range(die.getSides()):
	rolls.append(0)
#Roll 100 times the number of sides
for x in range(100 * die.getSides()):
	num = die.roll()
	rolls[num - 1] += 1		#You add one to the number directly behind the current number. This accounts for starting at subscript 0
#Print the results
cnt = 0
for num in rolls:
	print(str(cnt + 1) + ". " + str(num))
	cnt += 1
