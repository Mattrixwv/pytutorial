#This is a simple test on how to import files and use their functions and classes in python

#Import the functions from the file
#These first two import specific functions from a file
#from testImport import testFunction
#from testImport import testFunction2
#This command imports every function in the file
from testImport import *

print(testFunction())
print("The output of the second function is " + str(testFunction2(3, 4)))
